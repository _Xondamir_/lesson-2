function myFunction(a, b)
 {
    const result={};
    for(let i=0;i<a.length;i++)
    {
      result[a[i]]=b[i];
    }
    return result;
  }
  
  myFunction(['a', 'b', 'c'], [1, 2, 3]);
  myFunction(['w', 'x', 'y', 'z'], [10, 9, 5, 2]);
  myFunction([1, 'b'], ['a', 2]);
  