function Factory(products, price)
{
    return {
        products,
        price
    }
}
function Product(products, price){
    this.products = products;
    this.price = price;
}

let popular = new Product('лаваш мясной слаssис','картофел-фри', 35000);
let retro= new Product('шаурма мясной слаssис','картофел-фри', 35000);
let traditional= new Product('клаб-сендвич','картофел-фри', 35000);
let trend = new Product(['гамбургер','картофел-фри'], 35000);

console.log(popular,retro,traditional,trend);